package com.gautam.databse;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends Activity implements OnClickListener {
	private SQLiteAdapter mySQLiteAdapter;
	private EditText name;
	private Button add;
	ListView listContent;

	/** Called when the activity is first created. */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listContent = (ListView) findViewById(R.id.contentlist);
		name = (EditText) findViewById(R.id.editText1);
		add = (Button) findViewById(R.id.button1);
		add.setOnClickListener(this);

	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		mySQLiteAdapter = new SQLiteAdapter(this);
		mySQLiteAdapter.openToWrite();
		mySQLiteAdapter.insert(name.getText().toString());

		Cursor cursor = mySQLiteAdapter.queueAll();
		startManagingCursor(cursor);

		String[] from = new String[] { SQLiteAdapter.KEY_COLUMN1,
				SQLiteAdapter.KEY_COLUMN2, SQLiteAdapter.KEY_COLUMN3 };
		int[] to = new int[] { R.id.text, R.id.text, R.id.text };

		SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this,
				R.layout.row, cursor, from, to);
		listContent.setAdapter(cursorAdapter);
		cursorAdapter.notifyDataSetChanged();
		mySQLiteAdapter.close();
	}
}