package com.gautam.databse;

/**
 * Entertainment contains the feed link to the this Category
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 * @Version 1
 */

import java.util.ArrayList;

/**
 * Entertainment contains the feed link to the this Category
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 * @Version 1
 */
public class Entertainment {
	/**
	 * mName contains the Tag
	 */
	public static String mName = "Entertainment";
	/**
	 * mLink contains the link to the Tag
	 */
	public static ArrayList<String> mLink = new ArrayList<String>();

	/**
	 * mLinkFlags contains the link to the Tag
	 */
	public static boolean[] mLinkFlags = { false, true, false, true };

	/**
	 * Changes the flag
	 * 
	 * @param value
	 * @param isChecked
	 */
	public static void ChangemLinkFlage(String value, boolean isChecked) {

		mLinkFlags[mLink.indexOf(value)] = isChecked;

	}

	/**
	 * Entertainment feeds links area added
	 */
	static {
		mLink.add("http://feeds.feedburner.com/NDTV-Ent1");
		mLink.add("http://feeds.feedburner.com/NDTV-Ent2");
		mLink.add("http://feeds.feedburner.com/NDTV-Ent3");
		mLink.add("http://feeds.feedburner.com/NDTV-Ent4");

	}

}
